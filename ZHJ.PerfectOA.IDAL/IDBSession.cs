﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.IDAL
{
    public partial interface IDBSession
    {
        //IUserInfoDal UserInfoDal { get; set; }
        DbContext Db { get; }
        bool SaveChanges();

        int ExecuteSql(string sql, params System.Data.SqlClient.SqlParameter[] pars);
        List<T> ExecuteQuerySql<T>(string sql, params System.Data.SqlClient.SqlParameter[] pars);
    }
}
