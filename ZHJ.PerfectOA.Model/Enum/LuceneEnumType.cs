﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.Model.Enum
{
   public enum LuceneEnumType
    {
       /// <summary>
       /// 添加
       /// </summary>
       Add,
       /// <summary>
       /// 删除
       /// </summary>
       Delete
    }
}
