﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.Model.Enum
{
    public enum ActionInfoTypeEnum
    {
        /// <summary>
        /// 菜单权限
        /// </summary>
        MenuActionTypeEnum = 1,
        /// <summary>
        /// 普通权限
        /// </summary>
        NormalActionTypeEnum = 0
    }
}
