﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.BLL
{
    public partial class R_UserInfo_ActionInfoService:BaseService<R_UserInfo_ActionInfo>,IR_UserInfo_ActionInfoService
    {
        /// <summary>
        /// 完成特殊用户对权限的分配
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="actionId"></param>
        /// <param name="isPass"></param>
        /// <returns></returns>
        public bool SetUserInfoActionInfo(int userId, int actionId, bool isPass)
        {
            //根据用户id得到用户，根据权限id得到对应要修改的权限
            var user_actionInfo = this.GetDbSession.R_UserInfo_ActionInfoDal.LoadEntities(r => r.UserInfoID == userId && r.ActionInfoID == actionId).FirstOrDefault();
            if (user_actionInfo == null)
            {
                //如果找到的话，重新赋值修改
                R_UserInfo_ActionInfo r_UserInfo_ActionInfo = new R_UserInfo_ActionInfo();
                r_UserInfo_ActionInfo.UserInfoID = userId;
                r_UserInfo_ActionInfo.ActionInfoID = actionId;
                r_UserInfo_ActionInfo.IsPass = isPass;
                this.GetDbSession.R_UserInfo_ActionInfoDal.AddEntity(r_UserInfo_ActionInfo);
            }
            else
            {
                user_actionInfo.IsPass = isPass;
            }
            return this.GetDbSession.SaveChanges();
        }
    }
}
