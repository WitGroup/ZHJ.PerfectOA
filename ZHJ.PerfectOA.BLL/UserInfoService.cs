﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.BLL
{
    public partial class UserInfoService : BaseService<UserInfo>, IUserInfoService
    {
        /// <summary>
        /// 重写父类中的方法，在DbSession中找到UserInfoDal赋值给父类中的CurrentDal属性
        /// </summary>
        //public override void SetCurrentDal()
        //{
        //    CurrentDal = this.GetDbSession.UserInfoDal;
        //}

        /// <summary>
        /// 批量删除记录
        /// </summary>
        /// <param name="list">要删除的记录编号(ID)</param>
        /// <returns></returns>
        public bool DeleteEntities(List<int> list)
        {
            var userInfoList = this.GetDbSession.UserInfoDal.LoadEntities(u => list.Contains(u.ID));
            foreach (var userInfo in userInfoList)
            {
                this.GetDbSession.UserInfoDal.DeleteEntity(userInfo);
            }
            return this.GetDbSession.SaveChanges();
        }

        /// <summary>
        /// 修改用户的角色信息
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool SetUserInfoRole(int userId, List<int> list)
        {
            //获取用户ID为 userID的信息
            var userInfo = this.GetDbSession.UserInfoDal.LoadEntities(u => u.ID == userId).FirstOrDefault();
            if (userInfo!=null)
            {
                //删除该用户的已有的角色，直接用导航属性的清除
                userInfo.RoleInfo.Clear();
                //遍历选中的角色
                foreach (int roleId in list)
                {
                    //查询角色信息
                    var roleInfo = this.GetDbSession.RoleInfoDal.LoadEntities(r => r.ID == roleId).FirstOrDefault();
                    //将查询到的角色信息加到userinfo的导航属性中
                    userInfo.RoleInfo.Add(roleInfo);
                }
            }
            return this.GetDbSession.SaveChanges();
        }
    }
}
