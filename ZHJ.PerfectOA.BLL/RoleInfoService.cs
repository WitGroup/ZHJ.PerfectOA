﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.BLL
{
    public partial class RoleInfoService : BaseService<RoleInfo>, IRoleInfoService
    {

        public bool DeleteEntities(List<int> list)
        {
            var roleInfoDalList = this.GetDbSession.RoleInfoDal.LoadEntities(u => list.Contains(u.ID));
            foreach (var roleInfo in roleInfoDalList)
            {
                this.GetDbSession.RoleInfoDal.DeleteEntity(roleInfo);
            }
            return this.GetDbSession.SaveChanges();
        }
    }
}
