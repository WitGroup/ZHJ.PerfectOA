﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.IBLL
{
    public partial interface IR_UserInfo_ActionInfoService:IBaseService<R_UserInfo_ActionInfo>
    {
        bool SetUserInfoActionInfo(int userId,int actionId,bool isPass);
    }
}
