﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.IBLL
{
    public partial interface IKeyWordsRankService : IBaseService<KeyWordsRank>
    {
        void DeleteKeyWord();
        void InsertKeyWord();
        List<string> GetTerm(string msg);
    }
}
