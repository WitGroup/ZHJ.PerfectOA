﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.Quartz
{
    public class IndexJob:IJob
    {
        IBLL.IKeyWordsRankService KeyWordsRankService = new BLL.KeyWordsRankService();

        public void Execute(JobExecutionContext context)
        {
            KeyWordsRankService.DeleteKeyWord();
            KeyWordsRankService.InsertKeyWord();
        }
    }
}
