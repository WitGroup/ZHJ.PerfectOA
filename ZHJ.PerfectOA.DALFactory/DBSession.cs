﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.DAL;
using ZHJ.PerfectOA.IDAL;

namespace ZHJ.PerfectOA.DALFactory
{
    /// <summary>
    ///  数据会话层：实际就是一个工厂类，负责数据操作类实例的创建，然后将业务层与数据层解耦.业务层调用的数据会话层，
    ///  通过数据会话层，业务层就可以获取到操作的数据类的实例。
    /// </summary>
    public partial class DBSession : IDBSession
    {
        /// <summary>
        /// 得到上下文对象
        /// </summary>
        public System.Data.Entity.DbContext Db
        {
            get { return DbContextFactory.CreateCurrentDbContext(); }
        }

        //private IUserInfoDal _userInfoDal;
        ///// <summary>
        ///// 用反射的形式来动态的为_userInfoDal字段赋上实例
        ///// </summary>
        //public IUserInfoDal UserInfoDal
        //{
        //    get
        //    {
        //        //如果_userInfoDal==null 则为它创建实例
        //        if (_userInfoDal==null)
        //        {
        //            _userInfoDal = AbstractFactory.CreateUserInfoDal();
        //        }
        //        return _userInfoDal;
        //    }
        //    set
        //    {
        //        _userInfoDal = value;
        //    }
        //}

       
        /// <summary>
        /// 保存模型中的打上操作标签的数据
        /// </summary>
        /// <returns></returns>
        public bool SaveChanges()
        {
            return Db.SaveChanges() > 0;
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public int ExecuteSql(string sql, params System.Data.SqlClient.SqlParameter[] pars)
        {
            return Db.Database.ExecuteSqlCommand(sql, pars);
        }

        public List<T> ExecuteQuerySql<T>(string sql, params System.Data.SqlClient.SqlParameter[] pars)
        {
            return Db.Database.SqlQuery<T>(sql, pars).ToList();
        }
    }
}
