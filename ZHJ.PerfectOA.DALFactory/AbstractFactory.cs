﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using ZHJ.PerfectOA.IDAL;

namespace ZHJ.PerfectOA.DALFactory
{
    public partial class AbstractFactory
    {
        //private static readonly string DalAssemblyPath = ConfigurationManager.AppSettings["DalAssemblyPath"];
        //private static readonly string DalNameSpace = ConfigurationManager.AppSettings["DalNameSpace"];

        ///// <summary>
        ///// 用反射的方法来寻找类 创建实例
        ///// </summary>
        ///// <returns></returns>
        //public static IDAL.IUserInfoDal CreateUserInfoDal()
        //{
        //    string fullClassName = DalNameSpace + ".UserInfoDal";
        //    return CreateInstance(DalAssemblyPath, fullClassName) as IUserInfoDal;
        //}

        private static object CreateInstance(string DalAssemblyPath, string fullClassName)
        {
            var assembly = Assembly.Load(DalAssemblyPath);
            return assembly.CreateInstance(fullClassName);
        }

    }
}
