﻿using log4net;
using Spring.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ZHJ.PerfectOA.Common;
using ZHJ.PerfectOA.WebApp.Models;

namespace ZHJ.PerfectOA.WebApp
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : SpringMvcApplication
    {
        protected void Application_Start()
        {
            //开启一个线程，扫描队列，如果队列中有数据，将数据写到Lucene.Net文件中。
            IndexManager.GetInstance().StartThread();
            //读取配置文件 Log4.net内容
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);   //路由注册
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //string filePath = Server.MapPath("/Log/");//获取物理路径.
            ThreadPool.QueueUserWorkItem((a) =>
            {
                while (true)
                {
                    //如果异常队列中有值
                    if (MyExcetpionAttribute.ExceptionQueue.Count > 0)
                    {
                        //获取到异常数据，并且在队列中消除这个数据
                        Exception ex = MyExcetpionAttribute.ExceptionQueue.Dequeue();
                        //LogHelper.WriteLog(typeof(Exception), ex);
                        //LogHelper.WriteLog(typeof(MvcApplication), ex);
                        //ILog logger = LogManager.GetLogger("demo");
                        //logger.Error(ex);
                        LogHelper.WriteLog(typeof(Exception), ex);
                        //ILog logger = LogManager.GetLogger("Demo");
                        //logger.Error(ex);//将错误信息写到Log4Net指定的日志文件中。
                    }
                    else
                    {
                        //将线程睡眠3秒，避免资源浪费，避免了造成CPU空转。
                        Thread.Sleep(3000);
                    }
                }
            });

        }
    }
}