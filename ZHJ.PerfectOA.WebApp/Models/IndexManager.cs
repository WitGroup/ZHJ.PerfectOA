﻿using Lucene.Net.Analysis.PanGu;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using ZHJ.PerfectOA.Model;
using ZHJ.PerfectOA.Model.Enum;

namespace ZHJ.PerfectOA.WebApp.Models
{
    public class IndexManager
    {
        private readonly static IndexManager indexManager = new IndexManager();
        private IndexManager()
        {

        }

        public static IndexManager GetInstance()
        {
            return indexManager;
        }
        /// <summary>
        /// 定义队列 类型为IndexContent，用来储存要操作的数据
        /// </summary>
        public Queue<IndexContent> queue = new Queue<IndexContent>();

        /// <summary>
        /// 向队列中添加数据
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="title">标题</param>
        /// <param name="msg">内容</param>
        public void AddQueue(string id,string title,string msg)
        {
            IndexContent indexContent = new IndexContent();
            indexContent.Id = id;
            indexContent.Title = title;
            indexContent.Msg = msg;
            indexContent.LuceneEnumType = Model.Enum.LuceneEnumType.Add;
            queue.Enqueue(indexContent);
        }

        /// <summary>
        /// 删除队列中的数据
        /// </summary>
        /// <param name="id">要删除数据的id</param>
        public void DeleteQueue(string id)
        {
            IndexContent indexContent = new IndexContent();
            indexContent.Id = id;
            indexContent.LuceneEnumType = Model.Enum.LuceneEnumType.Delete;
            queue.Enqueue(indexContent);
        }

        /// <summary>
        /// 开一个线程执行CreateSearchIndex方法，创建Lucene文件
        /// </summary>
        public void StartThread()
        {
            Thread myThread = new Thread(CreateSearchIndex);
            myThread.IsBackground = true;
            myThread.Start();
        }

        /// <summary>
        /// 执行死循环，检测队列中是否要数据，如果有执行WriteIndexContent()，创建Lucene文件，如果没有线程睡眠5秒
        /// </summary>
        private void CreateSearchIndex()
        {
            while (true)
            {
                if (queue.Count>0)
                {
                    WriteIndexContent();
                }
                else
                {
                    Thread.Sleep(5000);//线程睡眠5秒，避免造成CPU空转
                }
            }
        }

        /// <summary>
        /// 创建Lucene文件
        /// </summary>
        private void WriteIndexContent()
        {
            string indexPath = @"C:\lucenedir";//注意和磁盘上文件夹的大小写一致，否则会报错。将创建的分词内容放在该目录下。
            FSDirectory directory = FSDirectory.Open(new DirectoryInfo(indexPath), new NativeFSLockFactory());//指定索引文件(打开索引目录) FS指的是就是FileSystem
            bool isUpdate = IndexReader.IndexExists(directory);//IndexReader:对索引进行读取的类。该语句的作用：判断索引库文件夹是否存在以及索引特征文件是否存在。
            if (isUpdate)
            {
                //同时只能有一段代码对索引库进行写操作。当使用IndexWriter打开directory时会自动对索引库文件上锁。
                //如果索引目录被锁定（比如索引过程中程序异常退出），则首先解锁（提示一下：如果我现在正在写着已经加锁了，但是还没有写完，这时候又来一个请求，那么不就解锁了吗？这个问题后面会解决）
                if (IndexWriter.IsLocked(directory))
                {
                    IndexWriter.Unlock(directory);
                }
            }
            IndexWriter writer = new IndexWriter(directory, new PanGuAnalyzer(), !isUpdate, Lucene.Net.Index.IndexWriter.MaxFieldLength.UNLIMITED);//向索引库中写索引。这时在这里加锁。

            //List<Books> list = BookService.LoadEntities(c => true).ToList();
            //如果队列中的个数>0
            while (queue.Count>0)
            {
                //在队列中取出数据
                IndexContent indexContent = queue.Dequeue();
                //删除数据
                writer.DeleteDocuments(new Term("Id", indexContent.Id.ToString()));
                //判断如果标识是删除，边跳到循环条件执行下次的判断，如果不是那就是添加或者修改，那么直接执行下边的添加方法
                if (indexContent.LuceneEnumType == LuceneEnumType.Delete)
                {
                    continue;
                }

                Document document = new Document();//表示一篇文档。
                //Field.Store.YES:表示是否存储原值。只有当Field.Store.YES在后面才能用doc.Get("number")取出值来.Field.Index. NOT_ANALYZED:不进行分词保存
                document.Add(new Field("Id", indexContent.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));

                //Field.Index. ANALYZED:进行分词保存:也就是要进行全文的字段要设置分词 保存（因为要进行模糊查询）

                //Lucene.Net.Documents.Field.TermVector.WITH_POSITIONS_OFFSETS:不仅保存分词还保存分词的距离。
                document.Add(new Field("Title", indexContent.Title, Field.Store.YES, Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.WITH_POSITIONS_OFFSETS));
                document.Add(new Field("Msg", indexContent.Msg, Field.Store.YES, Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.WITH_POSITIONS_OFFSETS));

                writer.AddDocument(document);

            }
            writer.Close();//会自动解锁。
            directory.Close();//不要忘了Close，否则索引结果搜不到
        }
    }
}