﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZHJ.PerfectOA.WebApp.Models
{
    /// <summary>
    /// 自定义异常过滤器，捕获方法的异常。
    /// </summary>
    public class MyExcetpionAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 定义一个队列，用来保存异常信息
        /// </summary>
        public static Queue<Exception> ExceptionQueue = new Queue<Exception>();
        public override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            ExceptionQueue.Enqueue(ex);//写入队列.
            filterContext.HttpContext.Response.Redirect("/Error.html");
            base.OnException(filterContext);
        }
    }
}