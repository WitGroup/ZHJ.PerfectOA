﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZHJ.PerfectOA.Model.Enum;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.WebApp.Controllers
{
    public class UserInfoController : Controller
    {
        //
        // GET: /UserInfo/
        //IBLL.IUserInfoService UserInfoServer = new BLL.UserInfoService();
        IBLL.IUserInfoService UserInfoService { get; set; }
        IBLL.IRoleInfoService RoleInfoService { get; set; }
        IBLL.IActionInfoService ActionInfoService { get; set; }
        IBLL.IR_UserInfo_ActionInfoService R_UserInfo_ActionInfoService { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        #region 分页显示用户信息数据
        /// <summary>
        /// 分页获取数据
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserInfo()
        {
            int pageIndex = Request["page"] != null ? int.Parse(Request["page"]) : 1;
            int pageSize = Request["rows"] != null ? int.Parse(Request["rows"]) : 5;
            int totalCount;
            short delflage = (short)DelFlags.Normal;
            var userInfoList = UserInfoService.LoadPageEntities<int>(pageIndex, pageSize, out totalCount, c => c.DelFlag == delflage, c => c.ID, true);
            var temp = from u in userInfoList
                       select new { ID = u.ID, UserName = u.UName, UserPass = u.UPwd, Remark = u.Remark, RegTime = u.SubTime };
            //将数据JSON化之后返回客户端，并且制定是来自客户端的Get请求方法的
            return Json(new { rows = temp, total = totalCount }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 添加用户操作
        /// <summary>
        /// 添加用户操作
        /// </summary>
        /// <returns></returns>
        public ActionResult AddUserInfo(UserInfo userinfo)
        {
            //将数据补充完整
            userinfo.DelFlag = 0;
            userinfo.SubTime = DateTime.Now;
            userinfo.ModifiedOn = DateTime.Now;
            //将补充完整的数据提交到BLL
            UserInfoService.AddEntity(userinfo);
            //成功之后返回OK
            return Content("ok");
        }
        #endregion

        #region 删除操作
        /// <summary>
        /// 删除操作
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteUserInfo()
        {
            //分割id字符串 将每个id加到list集合中
            string strId = Request["strId"];
            string[] ids = strId.Split(',');
            List<int> list = new List<int>();
            foreach (var id in ids)
            {
                list.Add(int.Parse(id));
            }

            if (UserInfoService.DeleteEntities(list))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }
        #endregion

        #region 修改操作---获取要修改的信息
        /// <summary>
        /// 修改操作---获取要修改的信息
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserInfoModel()
        {
            int id = int.Parse(Request["id"]);
            UserInfo userinfo = UserInfoService.LoadEntities(u => u.ID == id).FirstOrDefault();

            return Json(userinfo, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 修改用户信息
        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        public ActionResult EditUserInfo(UserInfo userinfo)
        {
            userinfo.DelFlag = 0;
            userinfo.ModifiedOn = DateTime.Now;
            if (UserInfoService.EditEntity(userinfo))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }
        #endregion

        #region 展示和修改用户的角色信息
        /// <summary>
        /// 展示用户的角色信息
        /// </summary>
        /// <returns></returns>
        public ActionResult SetUserRoleInfo()
        {
            int userId = int.Parse(Request["id"]);
            var userInfo = UserInfoService.LoadEntities(u => u.ID == userId).FirstOrDefault();
            ViewBag.UserInfo = userInfo;
            //获取所有的角色信息
            short delFlag = (short)DelFlags.Normal;
            var AllRole = RoleInfoService.LoadEntities(r => r.DelFlag == delFlag).ToList();
            //当用户已经具有的角色
            var AllExtRoleId = (from r in userInfo.RoleInfo
                                select r.ID).ToList();
            ViewBag.AllRole = AllRole;
            ViewBag.AllRoleId = AllExtRoleId;
            return View();
        }

        /// <summary>
        /// 给用户分配角色信息
        /// </summary>
        /// <param name="roleInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetUserRoleInfo(RoleInfo roleInfo)
        {
            int userId = int.Parse(Request["userId"]);
            //获取所有表单中的元素
            string[] allKeys = Request.Form.AllKeys;
            List<int> list = new List<int>();
            foreach (string key in allKeys)
            {
                //获取包含name包含cha_的值
                if (key.StartsWith("cba_"))
                {
                    string k = key.Replace("cba_", "");
                    list.Add(int.Parse(k));
                }
            }
            if (UserInfoService.SetUserInfoRole(userId, list))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }
        #endregion


        #region 为特殊用户分配权限
        /// <summary>
        /// 为特殊用户分配权限-渲染模板
        /// </summary>
        /// <returns></returns>
        public ActionResult SetUserActionInfo()
        {
            int userId = int.Parse(Request["userId"]);
            var userInfo = UserInfoService.LoadEntities(u => u.ID == userId).FirstOrDefault();
            ViewData.Model = userInfo;
            ViewBag.AllActionInfo = ActionInfoService.LoadEntities(a => true).ToList();
            ViewBag.AllActionId = userInfo.R_UserInfo_ActionInfo.ToList();//该用户具有的权限
            return View();
        }
        /// <summary>
        /// 为特殊用户分配权限-操作
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetUserActionInfo(UserInfo userInfo)
        {
            int userId = int.Parse(Request["userId"]);
            int actionId = int.Parse(Request["actionId"]);
            bool value = Request["value"] == "true" ? true : false;
            if (R_UserInfo_ActionInfoService.SetUserInfoActionInfo(userId, actionId, value))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }

        /// <summary>
        /// 清除用户的权限
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveUserAction()
        {
            int userId = int.Parse(Request["userId"]);
            int actionId = int.Parse(Request["actionId"]);
            var r_userInfo_actionInfo = R_UserInfo_ActionInfoService.LoadEntities(r => r.UserInfoID == userId && r.ActionInfoID == actionId).FirstOrDefault();
            if (R_UserInfo_ActionInfoService.DeleteEntity(r_userInfo_actionInfo))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }

        }
        #endregion
        
    }
}
