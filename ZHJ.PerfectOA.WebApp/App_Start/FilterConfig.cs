﻿using System.Web;
using System.Web.Mvc;
using ZHJ.PerfectOA.WebApp.Models;

namespace ZHJ.PerfectOA.WebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
       //     filters.Add(new HandleErrorAttribute());
            //注册自己的异常处理的过滤器
            filters.Add(new MyExcetpionAttribute());
        }
    }
}